#include <zephyr/kernel.h>
#include <lte_client/lte_client.h>

void main(void)
{
	
	printk("hello_http_post running on %s\n", CONFIG_BOARD);

	lte_client_connect();

	struct lte_client_gps_info gps;
	lte_client_get_gps_info(&gps);	

	while (1) {

		struct lte_client_net_info net;
		lte_client_get_net_info(&net);
	
#if defined(CONFIG_NET_SOCKETS_SOCKOPT_TLS)	
	lte_client_post("https","solans.web.cern.ch","443","/iotpm.php","cmd=add_measurement&sensor_id=2&temperature=22.3");
#else
	lte_client_post("http","solans.web.cern.ch","80","/iotpm.php","cmd=add_measurement&sensor_id=1&temperature=22.3");
#endif
	
		lte_client_power_saving(true);
		k_msleep(60000);
		lte_client_power_saving(false);
	}

}


