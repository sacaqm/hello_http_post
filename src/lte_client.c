#include <stdio.h>
#include <stdlib.h>
#include <zephyr/kernel.h>
#include <modem/modem_info.h>
#include <modem/lte_lc.h>
#include <zephyr/net/socket.h>
#include <zephyr/net/tls_credentials.h>
#include <nrf_modem_gnss.h>
#include <lte_client/lte_client.h>

/*
//Define certificate for peer verification
static const unsigned char ca_cern[] = {
#include "cern-ca.crt.inc"
};
*/

static struct nrf_modem_gnss_pvt_data_frame lte_client_pvt;

static void lte_client_gps_handler(int event)
{
	switch(event){
	case NRF_MODEM_GNSS_EVT_PVT:
		int num_satellites = 0;
		for (int i = 0; i < 12 ; i++) { if (lte_client_pvt.sv[i].signal != 0) { num_satellites++;}} 
		printk("LTE_CLIENT: GNSS found num satellites: %i\n", num_satellites);
		nrf_modem_gnss_read(&lte_client_pvt, sizeof(lte_client_pvt), NRF_MODEM_GNSS_DATA_PVT);
		if(lte_client_pvt.flags & NRF_MODEM_GNSS_PVT_FLAG_FIX_VALID){
			printk("LTE_CLIENT: update GPS coordinates\n");
			printk("Latitude:   %.06f\n", lte_client_pvt.latitude);
			printk("Longitude:  %.06f\n", lte_client_pvt.longitude);
			printk("Altitude:   %.01f m\n", lte_client_pvt.altitude);
			printk("Time (UTC): %02u:%02u:%02u.%03u\n",
				lte_client_pvt.datetime.hour, 
				lte_client_pvt.datetime.minute,
				lte_client_pvt.datetime.seconds,
				lte_client_pvt.datetime.ms);
		}
		break;
	case NRF_MODEM_GNSS_EVT_PERIODIC_WAKEUP:
		printk("LTE_CLIENT: GNSS has woken up\n");
		break;
	case NRF_MODEM_GNSS_EVT_SLEEP_AFTER_FIX:
		printk("LTE_CLIENT: GNSS enter sleep after geolocation\n");
		break;
	}
}

void lte_client_evt_handler(const struct lte_lc_evt * const evt)
{
	switch(evt->type){
	case LTE_LC_EVT_NEIGHBOR_CELL_MEAS:
		printk("LTE_CLIENT: Found num cells: %i\n",evt->cells_info.ncells_count);
		for(uint8_t i=0;i<evt->cells_info.ncells_count;i++){
			printk("cell %i: %i\n",i,evt->cells_info.neighbor_cells[i].phys_cell_id);
		}
		printk("LTE_CLIENT: Found num global cells: %i\n",evt->cells_info.gci_cells_count);
		for(uint8_t i=0;i<evt->cells_info.gci_cells_count;i++){
			printk("cell %i: %i\n",i,evt->cells_info.gci_cells[i].phys_cell_id);
		}
		break;
	default:
		break;
	}
}

bool lte_client_connect(){
	int err;
	char buf[80];
	
	err = lte_lc_init();
	if (err) printk("LTE_CLIENT: Failed initializing LTE Link controller, error: %d\n", err);
	
	err = lte_lc_func_mode_set(LTE_LC_FUNC_MODE_ACTIVATE_UICC);
	if (err) printk("LTE_CLIENT: Failed enabling UICC power, error: %d\n", err);
	
	err = modem_info_init();
	if (err) printk("LTE_CLIENT: Failed initializing modem info module, error: %d\n", err);
	
	if(modem_info_string_get(MODEM_INFO_FW_VERSION, buf, sizeof(buf))>0){printk("LTE_CLIENT: FW Version: %s\n",buf);}
	if(modem_info_string_get(MODEM_INFO_IMEI,       buf, sizeof(buf))>0){printk("LTE_CLIENT: IMEI: %s\n",buf);}
	if(modem_info_string_get(MODEM_INFO_ICCID,      buf, sizeof(buf))>0){printk("LTE_CLIENT: ICCID: %s\n",buf);}

	printk("LTE_CLIENT: Waiting for LTE network...\n");
	err = lte_lc_init_and_connect();
	if (err) {
		printk("LTE_CLIENT: Failed to connect to the LTE network, err %d\n", err);
		return false;
	}
	
	printk("LTE_CLIENT: Connected to LTE network\n");
	if(modem_info_string_get(MODEM_INFO_APN,        buf, sizeof(buf))>0){printk("LTE_CLIENT: APN: %s\n",buf);}
	if(modem_info_string_get(MODEM_INFO_IP_ADDRESS, buf, sizeof(buf))>0){printk("LTE_CLIENT: IP address: %s\n",buf);}
	if(modem_info_string_get(MODEM_INFO_RSRP,       buf, sizeof(buf))>0){printk("LTE_CLIENT: Signal strength: %s\n",buf);}
	if(modem_info_string_get(MODEM_INFO_AREA_CODE,  buf, sizeof(buf))>0){printk("LTE_CLIENT: AREA: %s\n",buf);}
	if(modem_info_string_get(MODEM_INFO_UE_MODE,    buf, sizeof(buf))>0){printk("LTE_CLIENT: MODE: %s\n",buf);}
	if(modem_info_string_get(MODEM_INFO_CELLID,     buf, sizeof(buf))>0){printk("LTE_CLIENT: CELL: %s\n",buf);}
	if(modem_info_string_get(MODEM_INFO_OPERATOR,   buf, sizeof(buf))>0){printk("LTE_CLIENT: OPE: %s\n",buf);}
	if(modem_info_string_get(MODEM_INFO_MCC,        buf, sizeof(buf))>0){printk("LTE_CLIENT: MCC: %s\n",buf);}
	if(modem_info_string_get(MODEM_INFO_MNC,        buf, sizeof(buf))>0){printk("LTE_CLIENT: MNC: %s\n",buf);}
	if(modem_info_string_get(MODEM_INFO_GPS_MODE,   buf, sizeof(buf))>0){printk("LTE_CLIENT: GPS: %s\n",buf);}
	
	return true;
}

void lte_client_get_net_info(struct lte_client_net_info * info){
	modem_info_string_get(MODEM_INFO_IMEI, info->imei, sizeof(info->imei));
	modem_info_string_get(MODEM_INFO_ICCID, info->iccid, sizeof(info->iccid));
	modem_info_string_get(MODEM_INFO_APN, info->apn, sizeof(info->apn));
	modem_info_string_get(MODEM_INFO_IP_ADDRESS, info->ip, sizeof(info->ip));
	modem_info_string_get(MODEM_INFO_AREA_CODE, info->area, sizeof(info->area));
	modem_info_string_get(MODEM_INFO_OPERATOR, info->operator, sizeof(info->operator));
	modem_info_string_get(MODEM_INFO_CELLID, info->cellid, sizeof(info->cellid));
}

void lte_client_get_triangulation_info(){
		
	printk("LTE_CLIENT: Register an LTE_EVENT handler\n");
	lte_lc_register_handler(lte_client_evt_handler);
	struct lte_lc_ncellmeas_params pp;
	pp.search_type=LTE_LC_NEIGHBOR_SEARCH_TYPE_EXTENDED_LIGHT;//LTE_LC_NEIGHBOR_SEARCH_TYPE_EXTENDED_COMPLETE;
	pp.gci_count=3;
	lte_lc_neighbor_cell_measurement_cancel();
	lte_lc_neighbor_cell_measurement(NULL);
	
	printk("LTE_CLIENT: Wait 10s to get a measurement from the network\n");
	k_msleep(10000);
	
	lte_lc_neighbor_cell_measurement_cancel();
	lte_lc_neighbor_cell_measurement(NULL);
	
	printk("LTE_CLIENT: Wait 30s to get a measurement from the network\n");
	k_msleep(30000);
	
	lte_lc_neighbor_cell_measurement_cancel();
	lte_lc_neighbor_cell_measurement(NULL);
	lte_lc_deregister_handler(lte_client_evt_handler);
}

void lte_client_init_gps_info(struct lte_client_gps_info * info){
	info->valid=false;
	info->latitude = 0.0;
	info->longitude = 0.0;
	info->altitude = 0.0;
	sprintf(info->latitude_string,  "%.06f", info->latitude);
	sprintf(info->longitude_string, "%.06f", info->longitude);
	sprintf(info->altitude_string,  "%.01f", info->altitude);
}

void lte_client_get_gps_info(struct lte_client_gps_info * info){
	
	//Falsify the incoming info
	info->valid=false;
  //Change the LTE function to normal for the GNSS to work
	//Keep the previous mode in memory
	enum lte_lc_func_mode mode;
	lte_lc_func_mode_get(&mode);
	lte_lc_func_mode_set(LTE_LC_FUNC_MODE_NORMAL);
	
	//struct nrf_modem_gnss_agps_data_location location;
	//location.altitude = 400;
	//nrf_modem_gnss_agps_write(&location, sizeof(location), NRF_MODEM_GNSS_AGPS_LOCATION);
	//nrf_modem_gnss_use_case_set(NRF_MODEM_GNSS_USE_CASE_MULTIPLE_HOT_START | NRF_MODEM_GNSS_USE_CASE_LOW_ACCURACY);
	
	//Enable low power mode of the LTE
	//GPS is time multiplexed with LTE
	//We need the LTE to be off to get a GNSS fix
	lte_lc_psm_req(true);
	
	nrf_modem_gnss_event_handler_set(lte_client_gps_handler);
	
	//Single shot mode: Try as soon as possible for 300 seconds
	nrf_modem_gnss_fix_interval_set(0);
	nrf_modem_gnss_fix_retry_set(300);
	
	printk("LTE_CLIENT: Start the GPS\n");
	nrf_modem_gnss_start();
	
	for(uint8_t i=0;i<30;i++){
		
		printk("LTE_CLIENT: Wait 10s for GPS location\n");
		k_msleep(10000);

		if(lte_client_pvt.flags & NRF_MODEM_GNSS_PVT_FLAG_FIX_VALID){
			printk("LTE_CLIENT: Found GPS location\n");
			info->valid=true;
			info->latitude = lte_client_pvt.latitude;
			info->longitude = lte_client_pvt.longitude;
			info->altitude = lte_client_pvt.altitude;
			sprintf(info->latitude_string,  "%.06f", info->latitude);
			sprintf(info->longitude_string, "%.06f", info->longitude);
			sprintf(info->altitude_string,  "%.01f", info->altitude);
			break;
		}
	
	}		
	
	//Return to normal power management
	lte_lc_psm_req(false);
	
	//Restore the original LTE funcion mode
	lte_lc_func_mode_set(mode);

	//Stop the GNSS
	nrf_modem_gnss_stop();
	
}

void lte_client_nslookup(const char * hostname, const char * port)
{
	
	struct addrinfo hints = {
		.ai_family = AF_UNSPEC,	
		.ai_socktype = SOCK_STREAM,
	};

	struct addrinfo *ai;
	int err = getaddrinfo(hostname, port, &hints, &ai);
	if (err) {
		printk("LTE_CLIENT: getaddrinfo() failed, err %d\n", errno);
	}else{
		for (; ai; ai = ai->ai_next) {
			if (ai->ai_addr->sa_family == AF_INET) {
				struct sockaddr_in * ta = (struct sockaddr_in *) ai->ai_addr;
				char ipv4[INET_ADDRSTRLEN];
				inet_ntop(AF_INET, &ta->sin_addr, ipv4, INET_ADDRSTRLEN);
				printk("LTE_CLIENT: ip=%s, port=%i\n", ipv4,htons(ta->sin_port));
			}
		}
	}
}


void lte_client_post(const char * schema, const char * host, const char * port, const char * url, const char * payload){
	
	printk("LTE_CLIENT: post: schema=%s,host=%s,port=%s,url=%s,payload=%s\n", schema, host, port, url, payload);
		
	printk("LTE_CLIENT: getaddrinfo\n");
	static struct addrinfo hints;
	struct addrinfo *ai;
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	getaddrinfo(host, port, &hints, &ai);
	char t_host[INET_ADDRSTRLEN];
	inet_ntop(AF_INET, &((struct sockaddr_in *)ai->ai_addr)->sin_addr, t_host, INET_ADDRSTRLEN);
	int t_port=htons(((struct sockaddr_in *)ai->ai_addr)->sin_port);
	printk("LTE_CLIENT: address info: ip=%s, port=%i\n", t_host, t_port );
	
	printk("LTE_CLIENT: create socket\n");
	int sock=-1;
	if(strcmp(schema,"https")==0){
		sock = socket(ai->ai_family, ai->ai_socktype, IPPROTO_TLS_1_2);
		setsockopt(sock, SOL_TLS, TLS_HOSTNAME, host, sizeof(host)-1);
  	/*
		//Add credentials for peer verification
		int err=0;
		tls_credential_add(1, TLS_CREDENTIAL_CA_CERTIFICATE,ca_cern, sizeof(ca_cern));
		printk("LTE_CLIENT: tls_credential_add size: %i\n",sizeof(ca_cern) );
		sec_tag_t sec_tag_list[] = {1,2,3,4,};
		setsockopt(sock, SOL_TLS, TLS_SEC_TAG_LIST, sec_tag_list, sizeof(sec_tag_list));
  	*/
		int verify=0;
		setsockopt(sock, SOL_TLS, TLS_PEER_VERIFY, &verify, sizeof(verify));		
	}else{
		sock = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
	}
	printk("LTE_CLIENT: socket=%i\n",sock);
	

	printk("LTE_CLIENT: connect\n");
	int conn=connect(sock, ai->ai_addr, ai->ai_addrlen);
	if(conn<0){
		printk("LTE_CLIENT: connect() failed with error: %d\n", errno);
		close(sock);return;
	}
	printk("LTE_CLIENT: connection=%i\n",conn);
	
	
	printk("LTE_CLIENT: create http_request\n");
	char http_req[1024];
	char http_rep[1024];
	sprintf(http_req, "%s", "GET ");
	strcat(http_req, url);
	strcat(http_req, "?");
	strcat(http_req, payload);
	strcat(http_req, " HTTP/1.1\n");
	strcat(http_req, "Host: ");
	strcat(http_req, host);
	strcat(http_req, "\n");
	strcat(http_req, "Accept: */*\n");
	strcat(http_req, "User-Agent: Sacaqm/LTE_CLIENT\n");
	strcat(http_req, "\n");
	printk("%s\n",http_req);
	
	printk("LTE_CLIENT: send http_request\n");
	send(sock, http_req, strlen(http_req), 0);

	printk("LTE_CLIENT: read http_reply\n");
	while(recv(sock, http_rep, strlen(http_rep) - 1,0) > 0){
		printk("%s\n",http_rep);
	}
		
	printk("LTE_CLIENT: close socket\n");
	close(sock);
	
}

void lte_client_power_saving(bool enable){
	lte_lc_psm_req(enable);
}
